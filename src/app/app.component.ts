import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'componentInteractionCase01';
  transmitter_rectangle: string = "rectangle";
  transmitter_triangle: string = "triangle";
  transmitter_circle: string = "circle";
  transmitter_diamond: string = "diamond";
}
