import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-receiver-component',
  templateUrl: './receiver-component.component.html',
  styleUrls: ['./receiver-component.component.scss']
})
export class ReceiverComponentComponent implements OnInit {

  @Input()
  receiver_rectangle: string;
  @Input()
  receiver_triangle: string;
  @Input()
  receiver_circle: string;
  @Input()
  receiver_diamond: string;
  constructor() { }

  ngOnInit() {
  }

}
